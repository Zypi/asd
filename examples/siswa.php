<?php
$page="siswa";
include"../header.php";
?>


    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
      <div class="container-fluid">
        <div class="header-body">
          <!-- Card stats -->
          
        </div>
      </div>
    </div>
    <div class="container-fluid mt--7">
      <!--table -->
      <div class="row">
        <div class="col">
          <div class="card shadow">
            <div class="card-header border-0">
              <h3 class="text-black mb-0">Data Siswa</h3>
              <a href="tambahsiswa.php?ID=<?php echo $row['ID']?>" onclick="return confirm('Tambah Siswa?');" class="btn btn-sm btn-info">Tambah</i></a>
           </div>
            <div class="table-responsive">
              <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">No</th>
                    <th scope="col">NIM</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Tanggal Lahir</th>
                    <th scope="col">Kelas</th>
                    <th scope="col">Aksi</th>
                  </tr>
                </thead>
                <!-- TBODY -->
                <tbody>
                  <tr>
                             <?php
                    include ("../koneksi.php");
                    $sql ="select * from siswa";
                    $result = mysqli_query ($db_link,$sql);
                    while ($row=mysqli_fetch_array($result)){
                    ?>
              
  <tr >
                      <td><?php echo $row['No'];?></td>
                      <td><?php echo $row['NIM'];?></td>
                      <td><?php echo $row['Nama'];?></td>
                      <td><?php echo $row['Tgl_Lahir'];?></td>
                      <td><?php echo $row['Kelas'];?></td>
                     
                      
                      <td>
                                    <a href="ubahsiswa.php?NIM=<?php echo $row['NIM']?>" onclick="return confirm('Item ini akan diubah?');" class="btn btn-sm btn-warning"></i>Ubah</a>
                                      <a href="hapussiswa.php?NIM=<?php echo $row['NIM']?>" onclick="return confirm('Item ini akan dihapus?');" class="btn btn-sm btn-danger"></i>Hapus</a>
                                      
                                  </td>
                              </tr>
                          <?php
                  }
                  ?>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      
<?php
$page="home";
include"../footer.php";
?>