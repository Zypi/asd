<?php
$page="tables";
include"../header.php";
?>


    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
      <div class="container-fluid">
        <div class="header-body">
          <!-- Card stats -->
          
        </div>
      </div>
    </div>
    <div class="container-fluid mt--7">
      <!-- Table -->

       <form action="tmbhpenerbit.php" method="post" class="form-horizontal">
                <div class="row">
                <div class="col-md-5">
                 <div class="form-group"></div>
                    <input class="form-control" placeholder="No" type="text" name="No_Penerbit" required/>
                  </div>
                </div>
                <div class="form-group">
                <div class="row">
                <div class="col-md-5">
                 <div class="form-group"></div>
                    <input class="form-control" placeholder="Kode Penerbit" type="text" name="Kode_Penerbit" required/>
                  </div>
                </div>
                <div class="row">
                <div class="col-md-5">
                 <div class="form-group"></div>
                    <input class="form-control" placeholder="Nama Penerbit" type="text" name="Nama_Penerbit" required/>
                  </div>
                </div>
                </div>
              
 
                <div class="controls">
                  <button type="submit" class="btn btn-bd btn-success">Tambah</button>
                </div>
              </div>
                  
          
      <!-- Dark table -->
      
      <!-- Footer -->
      <footer class="footer">
        <div class="row align-items-center justify-content-xl-between">
          <div class="col-xl-6">
            <div class="copyright text-center text-xl-left text-muted">
              &copy; 2018 <a href="https://www.creative-tim.com" class="font-weight-bold ml-1" target="_blank">Creative Tim</a>
            </div>
          </div>
          <div class="col-xl-6">
            <ul class="nav nav-footer justify-content-center justify-content-xl-end">
              <li class="nav-item">
                <a href="https://www.creative-tim.com" class="nav-link" target="_blank">Creative Tim</a>
              </li>
              <li class="nav-item">
                <a href="https://www.creative-tim.com/presentation" class="nav-link" target="_blank">About Us</a>
              </li>
              <li class="nav-item">
                <a href="http://blog.creative-tim.com" class="nav-link" target="_blank">Blog</a>
              </li>
              <li class="nav-item">
                <a href="https://github.com/creativetimofficial/argon-dashboard/blob/master/LICENSE.md" class="nav-link" target="_blank">MIT License</a>
              </li>
            </ul>
          </div>
        </div>
      </footer>
    </div>
  </div>
  <!--   Core   -->
  <script src="../assets/js/plugins/jquery/dist/jquery.min.js"></script>
  <script src="../assets/js/plugins/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
  <!--   Optional JS   -->
  <!--   Argon JS   -->
  <script src="../assets/js/argon-dashboard.min.js?v=1.1.0"></script>
  <script src="https://cdn.trackjs.com/agent/v3/latest/t.js"></script>
  <script>
    window.TrackJS &&
      TrackJS.install({
        token: "ee6fab19c5a04ac1a32a645abde4613a",
        application: "argon-dashboard-free"
      });
  </script>
</body>

</html>
