<?php
$page="tables";
include"../header.php";
?>


    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
      <div class="container-fluid">
        <div class="header-body">
          <!-- Card stats -->
          
        </div>
      </div>
    </div>
    <div class="container-fluid mt--7">
      <!--table -->
      <div class="row">
        <div class="col">
          <div class="card shadow">
            <div class="card-header border-0">
              <h3 class="text-black mb-0">Data Petugas</h3>
             </div>
            <div class="table-responsive">
              <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Nama Depan</th>
                    <th scope="col">Nama Belakang</th>
                    <th scope="col">Hak</th>
                    <th scope="col">Aksi</th>
                    <th scope="col"></th>
                  </tr>
                </thead>
                <!-- TBODY -->
                <tbody>
                  <tr>
                             <?php
                    include ("../koneksi.php");
                    $sql ="select * from petugas";
                    $result = mysqli_query ($db_link,$sql);
                    while ($row=mysqli_fetch_array($result)){
                    ?>
              
  <tr >
                      <td><?php echo $row['ID'];?></td>
                      <td><?php echo $row['NamaDepan'];?></td>
                      <td><?php echo $row['NamaBelakang'];?></td>
                      <td><?php echo $row['Password'];?></td>
                     
                      
                      <td>
                                    <a href="ubahadm.php?ID=<?php echo $row['ID']?>" onclick="return confirm('Item ini akan diubah?');" class="btn btn-sm btn-warning"></i>Ubah</a>
                                      <a href="hapusadm.php?ID=<?php echo $row['ID']?>" onclick="return confirm('Item ini akan dihapus?');" class="btn btn-sm btn-danger"></i>Hapus</a>
                                      
                                  </td>
                              </tr>
                          <?php
                  }
                  ?>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      
<?php
$page="home";
include"../footer.php";
?>